// DynaRobinMex.cpp : Defines the exported functions for the DLL application.
//

#define S_FUNCTION_NAME  EKF
#define S_FUNCTION_LEVEL 2

#define K_SAMPLE_TIME ssGetSFcnParam(S,0) //  SAMPLE_TIME

#define PORT_WIDTH 1 

#include <yvals.h>
#include <mex.h>
#include <time.h>
#include <simstruc.h>
#include <Windows.h>
#include "Estimator.h"



double SampleTime;
int pocetak;

static void mdlInitializeSizes(SimStruct *S)
{
	ssSetNumSFcnParams(S, 1);//This is the number of expected parameters your S-function block expects
	if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) return; //Parameter mismatch will be reported by Simulink 

	ssSetNumContStates(S, 0);
	ssSetNumDiscStates(S, 0);

	/* set number of input ports, their widths and direct feed thru flags */
	if (!ssSetNumInputPorts(S, 27)) return;
	ssSetInputPortWidth(S, 0, PORT_WIDTH * 28); //  x_state_0
	ssSetInputPortWidth(S, 1, PORT_WIDTH * 27 * 27); //  Covariance matrix P0
	ssSetInputPortWidth(S, 2, PORT_WIDTH * 27 * 27); //  Q0
	ssSetInputPortWidth(S, 3, PORT_WIDTH * 3 * 3); //  Qf
	ssSetInputPortWidth(S, 4, PORT_WIDTH * 3 * 3); //  Qbf
	ssSetInputPortWidth(S, 5, PORT_WIDTH * 3 * 3); //  Qw
	ssSetInputPortWidth(S, 6, PORT_WIDTH * 3 * 3); //  Qbw
	ssSetInputPortWidth(S, 7, PORT_WIDTH * 3 * 3); //  Qp1
	ssSetInputPortWidth(S, 8, PORT_WIDTH * 3 * 3); //  Qp2
	ssSetInputPortWidth(S, 9, PORT_WIDTH * 3 * 3); //  Qp3
	ssSetInputPortWidth(S, 10, PORT_WIDTH * 3 * 3); //  Qp4
	ssSetInputPortWidth(S, 11, PORT_WIDTH * 3 * 3); //  Rs1pom
	ssSetInputPortWidth(S, 12, PORT_WIDTH * 3 * 3); //  Rs2pom
	ssSetInputPortWidth(S, 13, PORT_WIDTH * 3 * 3); //  Rs3pom
	ssSetInputPortWidth(S, 14, PORT_WIDTH * 3 * 3); //  Rs4pom
	ssSetInputPortWidth(S, 15, PORT_WIDTH * 3 * 3); //  Ra1
	ssSetInputPortWidth(S, 16, PORT_WIDTH * 3 * 3); //  Ra2
	ssSetInputPortWidth(S, 17, PORT_WIDTH * 3 * 3); //  Ra3
	ssSetInputPortWidth(S, 18, PORT_WIDTH * 3 * 3); //  Ra4
	ssSetInputPortWidth(S, 26, PORT_WIDTH * 3 * 3); //  Initial rotation matrix

	// Measurements
	ssSetInputPortWidth(S, 19, PORT_WIDTH * 3); // Measured lin. acc. (f)
	ssSetInputPortWidth(S, 20, PORT_WIDTH * 3); // Measured ang. vel. (w)
	ssSetInputPortWidth(S, 21, PORT_WIDTH * 3); // Measured jointPos FL
	ssSetInputPortWidth(S, 22, PORT_WIDTH * 3); // Measured jointPos FR
	ssSetInputPortWidth(S, 23, PORT_WIDTH * 3); // Measured jointPos BL
	ssSetInputPortWidth(S, 24, PORT_WIDTH * 3); // Measured jointPos BR
	ssSetInputPortWidth(S, 25, PORT_WIDTH * 4); //  FootContact





												/* set the number of output ports and their widths */
	if (!ssSetNumOutputPorts(S, 9)) return;

	ssSetOutputPortWidth(S, 0, 3); //Estimated global Position
	ssSetOutputPortWidth(S, 1, 3); //Estimated global velocity
	ssSetOutputPortWidth(S, 2, 4); //Estimated rotation
	ssSetOutputPortWidth(S, 3, 3); //Foothold global position FL
	ssSetOutputPortWidth(S, 4, 3); //Foothold global position FR
	ssSetOutputPortWidth(S, 5, 3); //Foothold global position BL
	ssSetOutputPortWidth(S, 6, 3); //Foothold global position BR
	ssSetOutputPortWidth(S, 7, 27 * 27); //Current Covariance matrix
	ssSetOutputPortWidth(S, 8, 3*3); //Current Rotation Matrix


	ssSetNumSampleTimes(S, 1);
	ssSetNumRWork(S, 0);
	ssSetNumIWork(S, 0);
	ssSetNumPWork(S, 2); // OdeWorld
	ssSetNumModes(S, 0);
	ssSetNumNonsampledZCs(S, 0);

	ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);
	ssSetOptions(S, (SS_SIM | SS_GENERATED_S_FUNCTION));

}

static void mdlInitializeSampleTimes(SimStruct *S)
{
	ssSetSampleTime(S, 0, mxGetScalar(K_SAMPLE_TIME));
	ssSetOffsetTime(S, 0, 0.0);
}


#define MDL_START
static void mdlStart(SimStruct *S)
{

	SampleTime = (double)mxGetPr(K_SAMPLE_TIME)[0];
	pocetak = 1;

	




	//INIT Estimator
	Estimator *estimator = new Estimator(SampleTime);



	ssSetPWorkValue(S, 0, estimator);
}


#define MDL_UPDATE
static void mdlUpdate(SimStruct *S, int_T tid)
{
	Estimator *estimator = (Estimator*)ssGetPWorkValue(S, 0);


	if (pocetak == 1) {
		InputRealPtrsType X0 = ssGetInputPortRealSignalPtrs(S, 0); // uzmi sa ulaza
		InputRealPtrsType P0 = ssGetInputPortRealSignalPtrs(S, 1); // uzmi sa ulaza
		InputRealPtrsType Q0 = ssGetInputPortRealSignalPtrs(S, 2); // uzmi sa ulaza
		InputRealPtrsType Qf = ssGetInputPortRealSignalPtrs(S, 3); // uzmi sa ulaza
		InputRealPtrsType Qbf = ssGetInputPortRealSignalPtrs(S, 4); // uzmi sa ulaza
		InputRealPtrsType Qw = ssGetInputPortRealSignalPtrs(S, 5); // uzmi sa ulaza
		InputRealPtrsType Qbw = ssGetInputPortRealSignalPtrs(S, 6); // uzmi sa ulaza
		InputRealPtrsType Qp1 = ssGetInputPortRealSignalPtrs(S, 7); // uzmi sa ulaza
		InputRealPtrsType Qp2 = ssGetInputPortRealSignalPtrs(S, 8); // uzmi sa ulaza
		InputRealPtrsType Qp3 = ssGetInputPortRealSignalPtrs(S, 9); // uzmi sa ulaza
		InputRealPtrsType Qp4 = ssGetInputPortRealSignalPtrs(S, 10); // uzmi sa ulaza

		InputRealPtrsType Rs1 = ssGetInputPortRealSignalPtrs(S, 11); // uzmi sa ulaza
		InputRealPtrsType Rs2 = ssGetInputPortRealSignalPtrs(S, 12); // uzmi sa ulaza
		InputRealPtrsType Rs3 = ssGetInputPortRealSignalPtrs(S, 13); // uzmi sa ulaza
		InputRealPtrsType Rs4 = ssGetInputPortRealSignalPtrs(S, 14); // uzmi sa ulaza
		InputRealPtrsType Ra1 = ssGetInputPortRealSignalPtrs(S, 15); // uzmi sa ulaza
		InputRealPtrsType Ra2 = ssGetInputPortRealSignalPtrs(S, 16); // uzmi sa ulaza
		InputRealPtrsType Ra3 = ssGetInputPortRealSignalPtrs(S, 17); // uzmi sa ulaza
		InputRealPtrsType Ra4 = ssGetInputPortRealSignalPtrs(S, 18); // uzmi sa ulaza
		InputRealPtrsType C0 = ssGetInputPortRealSignalPtrs(S, 26); // uzmi sa ulaza



		estimator->Set_x0(Estimator::Array2Vector(28, (double *)X0));
		estimator->Set_P0(Estimator::Array2Matrix(27, 27, (double *)P0));
		estimator->Set_Q0(Estimator::Array2Matrix(27, 27, (double *)Q0));
		estimator->Set_Qf(Estimator::Array2Matrix(3, 3, (double *)Qf));
		estimator->Set_Qbf(Estimator::Array2Matrix(3, 3, (double *)Qbf));
		estimator->Set_Qw(Estimator::Array2Matrix(3, 3, (double *)Qw));
		estimator->Set_Qbw(Estimator::Array2Matrix(3, 3, (double *)Qbw));
		estimator->Set_Qp1(Estimator::Array2Matrix(3, 3, (double *)Qp1));
		estimator->Set_Qp2(Estimator::Array2Matrix(3, 3, (double *)Qp2));
		estimator->Set_Qp3(Estimator::Array2Matrix(3, 3, (double *)Qp3));
		estimator->Set_Qp4(Estimator::Array2Matrix(3, 3, (double *)Qp4));
		estimator->Set_Rs1pom(Estimator::Array2Matrix(3, 3, (double *)Rs1));
		estimator->Set_Rs2pom(Estimator::Array2Matrix(3, 3, (double *)Rs2));
		estimator->Set_Rs3pom(Estimator::Array2Matrix(3, 3, (double *)Rs3));
		estimator->Set_Rs4pom(Estimator::Array2Matrix(3, 3, (double *)Rs4));
		estimator->Set_Ra1(Estimator::Array2Matrix(3, 3, (double *)Ra1));
		estimator->Set_Ra2(Estimator::Array2Matrix(3, 3, (double *)Ra2));
		estimator->Set_Ra3(Estimator::Array2Matrix(3, 3, (double *)Ra3));
		estimator->Set_Ra4(Estimator::Array2Matrix(3, 3, (double *)Ra4));
		estimator->Set_C0(Estimator::Array2Matrix(3, 3, (double *)C0));
		estimator->Set_g();
		pocetak = 0;
	}


	InputRealPtrsType f = ssGetInputPortRealSignalPtrs(S, 19); // Measured lin. acc. (f)
	InputRealPtrsType w = ssGetInputPortRealSignalPtrs(S, 20); // Measured ang. vel. (w)
	InputRealPtrsType jointPos_FL = ssGetInputPortRealSignalPtrs(S, 21); // Measured jointPos FL
	InputRealPtrsType jointPos_FR = ssGetInputPortRealSignalPtrs(S, 22); // Measured jointPos FR
	InputRealPtrsType jointPos_BL = ssGetInputPortRealSignalPtrs(S, 23); // Measured jointPos BL
	InputRealPtrsType jointPos_BR = ssGetInputPortRealSignalPtrs(S, 24); // Measured jointPos BR
	InputRealPtrsType footContact = ssGetInputPortRealSignalPtrs(S, 25); //  FootContact

	estimator->Set_f_IMU(Estimator::Array2Vector(3, (double *)f));
	estimator->Set_w_IMU(Estimator::Array2Vector(3, (double *)w));
	estimator->Set_jointPos_FL(Estimator::Array2Vector(3, (double *)jointPos_FL));
	estimator->Set_jointPos_FR(Estimator::Array2Vector(3, (double *)jointPos_FR));
	estimator->Set_jointPos_BL(Estimator::Array2Vector(3, (double *)jointPos_BL));
	estimator->Set_jointPos_BR(Estimator::Array2Vector(3, (double *)jointPos_BR));
	estimator->Set_footContact(Estimator::Array2Vector(4, (double *)footContact));

	estimator->Prediction_step();
	estimator->Update_step();

}

static void mdlOutputs(SimStruct *S, int_T tid)
{

	Estimator *estimator = (Estimator*)ssGetPWorkValue(S, 0);

	if (pocetak == 1)
	{

	}
	else {
		real_T* r_out = ssGetOutputPortRealSignal(S, 0); //Estimated global Position
		real_T* v_out = ssGetOutputPortRealSignal(S, 1); //Estimated global velocity
		real_T* q_out = ssGetOutputPortRealSignal(S, 2); //Estimated rotation
		real_T* p1_out = ssGetOutputPortRealSignal(S, 3); //Foothold global position FL
		real_T* p2_out = ssGetOutputPortRealSignal(S, 4); //Foothold global position FR
		real_T* p3_out = ssGetOutputPortRealSignal(S, 5); //Foothold global position BL
		real_T* p4_out = ssGetOutputPortRealSignal(S, 6); //Foothold global position BR
		real_T* P_out = ssGetOutputPortRealSignal(S, 7); //Current Covariance matrix
		real_T* C_out = ssGetOutputPortRealSignal(S, 8); //Current Rotation Matrix

		VectorXd x_out = estimator->Get_x();
		MatrixXd P = estimator->Get_P();
		MatrixXd C = estimator->Get_C();

		VectorXd r(3), v(3), p1(3), q(4), p2(3), p3(3), p4(3), bf(3), bw(3);
		estimator->split_x(&r, &v, &q, &p1, &p2, &p3, &p4, &bf, &bw);

		memcpy(r_out, Estimator::Vector2Array(3, r), 3 * sizeof(double));
		memcpy(v_out, Estimator::Vector2Array(3, v), 3 * sizeof(double));
		memcpy(q_out, Estimator::Vector2Array(4, q), 4 * sizeof(double));
		memcpy(p1_out, Estimator::Vector2Array(3, p1), 3 * sizeof(double));
		memcpy(p2_out, Estimator::Vector2Array(3, p2), 3 * sizeof(double));
		memcpy(p3_out, Estimator::Vector2Array(3, p3), 3 * sizeof(double));
		memcpy(p4_out, Estimator::Vector2Array(3, p4), 3 * sizeof(double));
		memcpy(P_out, Estimator::Matrix2Array(27, 27, P), 27 * 27 * sizeof(double));
		memcpy(C_out, Estimator::Matrix2Array(3, 3, C), 27 * 27 * sizeof(double));
	}

}


static void mdlTerminate(SimStruct *S)
{
	Estimator *estimator = (Estimator*)ssGetPWorkValue(S, 0);
	delete estimator;
}



#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif