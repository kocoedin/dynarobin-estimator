#include "Estimator.h"



// Pripadaju klasi Estimator
Estimator::Estimator(double dt)
{
	m_dt = dt;
	
	m_x0 = VectorXd::Zero(28);
	m_P0 = MatrixXd::Zero(27,27);
	m_R0 = MatrixXd::Zero(12,12);
	m_Q0 = MatrixXd::Zero(27,27);
	m_C0 = MatrixXd::Zero(3, 3);
	m_g = VectorXd::Zero(3);
	m_x = VectorXd::Zero(28);
	m_P = MatrixXd::Zero(27, 27);
	m_R = MatrixXd::Zero(12, 12);
	m_C = MatrixXd::Zero(3, 3);

	m_Ra1 = MatrixXd::Zero(3, 3);
	m_Ra2 = MatrixXd::Zero(3, 3);
	m_Ra3 = MatrixXd::Zero(3, 3);
	m_Ra4 = MatrixXd::Zero(3, 3);
	m_Rs1 = MatrixXd::Zero(3, 3); m_Rs1pom = MatrixXd::Zero(3, 3);
	m_Rs2 = MatrixXd::Zero(3, 3); m_Rs2pom = MatrixXd::Zero(3, 3);
	m_Rs3 = MatrixXd::Zero(3, 3); m_Rs3pom = MatrixXd::Zero(3, 3);
	m_Rs4 = MatrixXd::Zero(3, 3); m_Rs4pom = MatrixXd::Zero(3, 3);
	m_Qf = MatrixXd::Zero(3, 3); m_Qbf = MatrixXd::Zero(3, 3);
	m_Qw = MatrixXd::Zero(3, 3); m_Qbw = MatrixXd::Zero(3, 3);
	m_Qp1 = MatrixXd::Zero(3, 3); m_Qp2 = MatrixXd::Zero(3, 3); m_Qp3 = MatrixXd::Zero(3, 3); m_Qp4 = MatrixXd::Zero(3, 3);
	m_w_IMU = VectorXd::Zero(3);
	m_f_IMU = VectorXd::Zero(3);
	m_jointPos_FL = VectorXd::Zero(3);
	m_jointPos_FR = VectorXd::Zero(3);
	m_jointPos_BL = VectorXd::Zero(3);
	m_jointPos_BR = VectorXd::Zero(3);
	m_footContact = VectorXd::Zero(4);
	

}

Estimator::~Estimator()
{
}

void Estimator::Set_x0(VectorXd value)
{
	m_x0 = value;
	m_x = value;
}

VectorXd Estimator::Get_x0(void)
{
	return m_x0;
}

void Estimator::Set_x(VectorXd value)
{
	m_x = value;
}

VectorXd Estimator::Get_x(void)
{
	return m_x;
}

void Estimator::Set_P0(MatrixXd value)
{
	m_P0 = value;
}

MatrixXd Estimator::Get_P0(void)
{
	return m_P0;
}

void Estimator::Set_Q0(MatrixXd value)
{
	m_Q0 = value;
}

MatrixXd Estimator::Get_Q0(void)
{
	return m_Q0;
}

void Estimator::Set_C0(MatrixXd value)
{
	m_C0 = value;
	m_C = value;
}

MatrixXd Estimator::Get_C0(void)
{
	return m_C0;
}

void Estimator::Set_C(MatrixXd value)
{
	m_C = value;
}

MatrixXd Estimator::Get_C(void)
{
	return m_C;
}
void Estimator::Set_R0(MatrixXd value)
{
	m_R0 = value;
}

MatrixXd Estimator::Get_R0(void)
{
	return m_R0;
}

void Estimator::Set_g(void)
{	
	m_g = VectorXd::Zero(3);
	//m_g = value;
}

VectorXd Estimator::Get_g(void)
{
	return m_g;
}

void Estimator::Set_P(MatrixXd value)
{
	m_P = value;
}

MatrixXd Estimator::Get_P(void)
{
	return m_P;
}

void Estimator::Set_R(MatrixXd value)
{
	m_R = value;
}

MatrixXd Estimator::Get_R(void)
{
	return m_R;
}

void Estimator::Set_Ra1(MatrixXd value)
{
	m_Ra1 = value;
}

MatrixXd Estimator::Get_Ra1(void)
{
	return m_Ra1;
}

void Estimator::Set_Ra2(MatrixXd value)
{
	m_Ra2 = value;
}

MatrixXd Estimator::Get_Ra2(void)
{
	return m_Ra2;
}

void Estimator::Set_Ra3(MatrixXd value)
{
	m_Ra3 = value;
}

MatrixXd Estimator::Get_Ra3(void)
{
	return m_Ra3;
}

void Estimator::Set_Ra4(MatrixXd value)
{
	m_Ra4 = value;
}

MatrixXd Estimator::Get_Ra4(void)
{
	return m_Ra4;
}

void Estimator::Set_Rs1pom(MatrixXd value)
{
	m_Rs1pom = value;
}

MatrixXd Estimator::Get_Rs1pom(void)
{
	return m_Rs1pom;
}

void Estimator::Set_Rs2pom(MatrixXd value)
{
	m_Rs2pom = value;
}

MatrixXd Estimator::Get_Rs2pom(void)
{
	return m_Rs2pom;
}

void Estimator::Set_Rs3pom(MatrixXd value)
{
	m_Rs3pom = value;
}

MatrixXd Estimator::Get_Rs3pom(void)
{
	return m_Rs3pom;
}

void Estimator::Set_Rs4pom(MatrixXd value)
{
	m_Rs4pom = value;
}

MatrixXd Estimator::Get_Rs4pom(void)
{
	return m_Rs4pom;
}

void Estimator::Set_Qf(MatrixXd value)
{
	m_Qf = value;
}

MatrixXd Estimator::Get_Qf(void)
{
	return m_Qf;
}

void Estimator::Set_Qbf(MatrixXd value)
{
	m_Qbf = value;
}

MatrixXd Estimator::Get_Qbf(void)
{
	return m_Qbf;
}

void Estimator::Set_Qw(MatrixXd value)
{
	m_Qw = value;
}

MatrixXd Estimator::Get_Qw(void)
{
	return m_Qw;
}

void Estimator::Set_Qbw(MatrixXd value)
{
	m_Qbw = value;
}

MatrixXd Estimator::Get_Qbw(void)
{
	return m_Qbw;
}

void Estimator::Set_Qp1(MatrixXd value)
{
	m_Qp1 = value;
}

MatrixXd Estimator::Get_Qp1(void)
{
	return m_Qp1;
}

void Estimator::Set_Qp2(MatrixXd value)
{
	m_Qp2 = value;
}

MatrixXd Estimator::Get_Qp2(void)
{
	return m_Qp2;
}

void Estimator::Set_Qp3(MatrixXd value)
{
	m_Qp3 = value;
}

MatrixXd Estimator::Get_Qp3(void)
{
	return m_Qp3;
}

void Estimator::Set_Qp4(MatrixXd value)
{
	m_Qp4 = value;
}

MatrixXd Estimator::Get_Qp4(void)
{
	return m_Qp4;
}


void Estimator::Set_w_IMU(VectorXd value)
{
	m_w_IMU = value;
}

VectorXd Estimator::Get_w_IMU(void)
{
	return m_w_IMU;
}

void Estimator::Set_f_IMU(VectorXd value)
{
	m_f_IMU = value;
}

VectorXd Estimator::Get_f_IMU(void)
{
	return m_f_IMU;
}

void Estimator::Set_jointPos_FL(VectorXd value)
{
	m_jointPos_FL = value;
}

VectorXd Estimator::Get_jointPos_FL(void)
{
	return m_jointPos_FL;
}

void Estimator::Set_jointPos_FR(VectorXd value)
{
	m_jointPos_FR = value;
}

VectorXd Estimator::Get_jointPos_FR(void)
{
	return m_jointPos_FR;
}

void Estimator::Set_jointPos_BL(VectorXd value)
{
	m_jointPos_BL = value;
}

VectorXd Estimator::Get_jointPos_BL(void)
{
	return m_jointPos_BL;
}

void Estimator::Set_jointPos_BR(VectorXd value)
{
	m_jointPos_BR = value;
}

VectorXd Estimator::Get_jointPos_BR(void)
{
	return m_jointPos_BR;
}

void Estimator::Set_footContact(VectorXd value)
{
	m_footContact = value;
	MatrixXd big_value = MatrixXd::Identity(3,3) * 99999;
	
	if (value(0) == 0) {
		m_Rs1 = big_value;
	}
	else {
		m_Rs1 = m_Rs1pom;
	}

	if (value(1) == 0) {
		m_Rs2 = big_value;
	}
	else {
		m_Rs2 = m_Rs2pom;
	}

	if (value(2) == 0) {
		m_Rs3 = big_value;
	}
	else {
		m_Rs3 = m_Rs3pom;
	}

	if (value(3) == 0) {
		m_Rs4 = big_value;
	}
	else {
		m_Rs4 = m_Rs4pom;
	}
}

VectorXd Estimator::Get_footContact(void)
{
	return m_footContact;
}

void Estimator::Prediction_step() {
	VectorXd x_p, r_p, v_p, p1_p, q_p2, p2_p, p3_p, p4_p, bf_p, bw_p;
	MatrixXd P_p;
	

	P_p = Get_P();
	x_p = Get_x();

	split_x(&r_p, &v_p, &q_p2, &p1_p, &p2_p, &p3_p, &p4_p, &bf_p, &bw_p);

	// Quaternionf q_m, q_p;
	//q_p = Quaternionf(q_p2(0), q_p2(1), q_p2(2), q_p2(3));
	// MatrixXd C_p = quat2rotm(q_p);

	MatrixXd C_p = m_C;
	MatrixXd C_p_T = C_p.transpose();
	VectorXd f_hat = Get_f_IMU() - bf_p;
	VectorXd w_hat = Get_w_IMU() - bw_p;

	// a priori state estimate equations 
	VectorXd r_m, v_m, q_m, p1_m, p2_m, p3_m, p4_m, bf_m, bw_m;
	r_m = r_p + m_dt*v_p + 0.5*pow(m_dt, 2)* (C_p_T*f_hat + m_g);
	
	v_m = v_p + m_dt*(C_p_T*f_hat + m_g);

	q_m = VectorXd::Zero(4);

	p1_m = p1_p;
	p2_m = p2_p;
	p3_m = p3_p;
	p4_m = p4_p;
	bf_m = bf_p;
	bw_m = bw_p;

MatrixXd rotation = eul2rotm(m_dt*w_hat);
	MatrixXd C_m = rotation*C_p;

	x_p = create_x(r_m, v_m, q_m, p1_m, p2_m, p3_m, p4_m, bf_m, bw_m);
	Set_x(x_p);

	MatrixXd F(27,27), Q(27,27);
	
	MatrixXd eye, F0235, F0268, F3568, F6868, F022123, F352123, F682426;

	eye = MatrixXd::Identity(3, 3);
	F0235 = m_dt*eye;

	F0268 = -m_dt*m_dt / 2 * C_p_T*skew_matrix(f_hat);
	F3568 = -m_dt * C_p_T*skew_matrix(f_hat);
	F6868 = map_gama(0, m_dt, w_hat).transpose();


	F022123 = -m_dt*m_dt / 2 * C_p_T;
	F352123 = -m_dt*C_p_T;

	F682426 = -map_gama(1, m_dt, w_hat).transpose();


	// first column
	F=set_matrix(F, 0, 2, 0, 2, eye);

	//second column
	F = set_matrix(F, 0, 2, 3, 5, F0235);
	F = set_matrix(F, 3, 5, 3, 5, eye);

	//third column
	F = set_matrix(F, 0, 2, 6, 8, F0268);
	F = set_matrix(F, 3, 5, 6, 8, F3568);
	F = set_matrix(F, 6, 8, 6, 8, F6868);

	//fourth column
	F = set_matrix(F, 9, 20, 9, 20, MatrixXd::Identity(12,12));

	//fifth column
	F = set_matrix(F, 0, 2, 21, 23, F022123);
	F = set_matrix(F, 3, 5, 21, 23, F352123);
	F = set_matrix(F, 21, 23, 21, 23, eye);

	//sixth column
	F = set_matrix(F, 6, 8, 25, 26, F682426);
	F=  set_matrix(F, 24, 26, 24, 26, eye);

	MatrixXd Q212302, Q212335, Q6868, Q242668, Q911911, Q12141214,
		Q15171517, Q18201820, Q022123, Q352123, Q682426;
	Q = m_Q0;

	Q212302 = -pow(m_dt, 3) / 6 * m_Qbf*C_p;

	Q212335 = -pow(m_dt, 2) / 2 * m_Qbf*C_p;

	Q6868 = m_dt*m_Qw + (map_gama(3, m_dt, w_hat) + map_gama(3, m_dt, w_hat).transpose())*m_Qbw;
	Q242668 = -m_Qbw*map_gama(2, m_dt, w_hat);

	Q911911 = m_dt*C_p_T*m_Qp1*C_p;
	Q12141214 = m_dt*C_p_T*m_Qp2*C_p;
	Q15171517 = m_dt*C_p_T*m_Qp3*C_p;
	Q18201820 = m_dt*C_p_T*m_Qp4*C_p;

	Q022123 = -pow(m_dt, 3) / 6 * C_p_T*m_Qbf;
	Q352123 = -pow(m_dt, 2) / 2 * C_p_T*m_Qbf;

	Q682426 = -1 * map_gama(2, m_dt, w_hat).transpose()*m_Qbw;

	// first column
	Q = set_matrix(Q, 21, 23, 0, 2, Q212302);

	//second column
	Q = set_matrix(Q, 21, 23, 3, 5, Q212335);

	//third column
	Q = set_matrix(Q, 6, 8, 6, 8, Q6868);
	Q = set_matrix(Q, 24, 26, 6, 8, Q242668);

	//fourth column
	Q = set_matrix(Q, 9, 11, 9, 1, Q911911);
	Q = set_matrix(Q, 12, 14, 12, 14, Q12141214);
	Q = set_matrix(Q, 15, 17, 15, 17, Q15171517);
	Q = set_matrix(Q, 18, 20, 18, 20, Q18201820);

	//fifth column
	Q = set_matrix(Q, 0, 2, 21, 23, Q022123);
	Q = set_matrix(Q, 3, 5, 21, 23, Q352123);

	//sixth column
	Q=set_matrix(Q, 6, 8, 24, 26, Q682426);

	MatrixXd P_m;
	P_m = F*P_p*F.transpose() + Q;
	Set_P(P_m);
	Set_C(C_m);
}

void Estimator::Update_step() {
	MatrixXd  H, R, S, K, P_p, I;
	VectorXd Y, Delta_x;

	VectorXd r_m, v_m, q_m2, p1_m, p2_m, p3_m, p4_m, bf_m, bw_m, x_p, x_m;
	MatrixXd P_m;

	//Quaternionf q_m;

	P_m = Get_P();
	x_m = Get_x();

	split_x(&r_m, &v_m, &q_m2, &p1_m, &p2_m, &p3_m, &p4_m, &bf_m, &bw_m);

	//q_m = Quaternionf(q_m2(0), q_m2(1), q_m2(2), q_m2(3));

	MatrixXd C_m = Get_C();

	Y = make_Y(Get_jointPos_FL(), Get_jointPos_FL(), Get_jointPos_FL(), Get_jointPos_FL(), C_m, r_m, p1_m, p2_m, p3_m, p4_m);
	H = make_H(C_m, r_m, p1_m, p2_m, p3_m, p4_m);
	R = make_R();
	I = Matrix<double, 27, 27>::Identity();

	S = H*m_P*H.transpose() + R;
	K = m_P*H.transpose()*S.inverse();
	Delta_x = K*Y;
	P_p = (I - K*H)*m_P;

	MatrixXd new_x = correctX(m_x, Delta_x);
	Set_P(P_p);
	Set_x(new_x);
	Set_C(correctC(Delta_x));

}

void Estimator::split_x(VectorXd * r, VectorXd * v, VectorXd *q, VectorXd * p1, VectorXd * p2, VectorXd * p3, VectorXd * p4, VectorXd * bf, VectorXd * bw)
{
	double _x[] = { m_x(0), m_x(1), m_x(2) };
	(*r) = Array2Vector(3, &_x[0]);

	double _v[] = { m_x(3), m_x(4), m_x(5) };
	(*v) = Array2Vector(3, &_v[0]);

	double _q[] = { m_x(6), m_x(7), m_x(8),  m_x(9) };
	(*q) = Array2Vector(4, &_q[0]);

	double _p1[] = { m_x(10), m_x(11), m_x(12) };
	(*p1) = Array2Vector(3, &_p1[0]);

	double _p2[] = { m_x(13), m_x(14), m_x(14) };
	(*p2) = Array2Vector(3, &_p2[0]);

	double _p3[] = { m_x(16), m_x(17), m_x(18) };
	(*p3) = Array2Vector(3, &_p3[0]);

	double _p4[] = { m_x(19), m_x(20), m_x(21) };
	(*p4) = Array2Vector(3, &_p4[0]);

	double _bf[] = { m_x(22), m_x(23), m_x(24) };
	(*bf) = Array2Vector(3, &_bf[0]);

	double _bw[] = { m_x[25], m_x[26], m_x[27] };
	(*bw) = Array2Vector(3, &_bw[0]);


}

VectorXd Estimator::create_x(VectorXd r, VectorXd v, VectorXd q, VectorXd p1, VectorXd p2, VectorXd p3, VectorXd p4, VectorXd bf, VectorXd bw)
{
	VectorXd x(28);
	for (int i = 0; i < 3; i++) {
		x(i) = r(i);
		x(3 + i) = v(i);
		x(6 + i) = q(i);
		x(10 + i) = p1(i);
		x(13 + i) = p2(i);
		x(16 + i) = p3(i);
		x(19 + i) = p4(i);
		x(22 + i) = bf(i);
		x(25 + i) = bw(i);
	}

	x(9) = q(3);
	return x;
}



VectorXd Estimator::Array2Vector(int Len, double *values) {

	VectorXd pom = VectorXd(Len);
	for (int i = 0; i < Len; i++) {
		pom(i) = *(values + i);
	}
	return pom;
}

double* Estimator::Vector2Array(int Len, VectorXd vector) {

	double *pom = (double*)malloc(Len * sizeof(double));
	for (int i = 0; i < Len; i++) {
		*(pom + i) = vector(i);
	}
	return pom;
}

double* Estimator::Matrix2Array(int Rows, int Columns, MatrixXd matrix) {

	double *pom = (double*)malloc(Rows*Columns * sizeof(double));
	for (int i = 0; i < Rows; i++) {
		for (int j = 0; j < Columns; j++) {
			*(pom + i*Columns + j) = matrix(i, j);
		}
	}
	return pom;
}

MatrixXd Estimator::Array2Matrix(int Rows, int Columns, double *values) {

	MatrixXd pom = MatrixXd(Rows, Columns);

	for (int i = 0; i < Rows; i++) {
		for (int j = 0; j < Columns; j++)
		{
			pom(i, j) = *(values + i*Rows + j);
		}
	}
	return pom;
}

MatrixXd Estimator::set_matrix(MatrixXd mat, int begin_row, int end_row, int begin_col, int end_col, MatrixXd mat2)
{
	MatrixXd pom(27, 27);
	pom = mat;
	int k = -1;
	int l = -1;
	for (int i = begin_row; i <= end_row; i++) {
		k = k + 1;
		l = -1;
		for (int j = begin_col; j <= end_col; j++) {
			l = l + 1;
			pom(i, j) = mat2(k, l);
		}
	}

	return pom;
}

MatrixXd Estimator::skew_matrix(VectorXd vector)
{
	MatrixXd pom(3,3);
	
	pom(0, 0) = 0; pom(1, 1) = 0; pom(2, 2) = 0;
	pom(0, 1) = -vector(2); pom(0, 2) = vector(1);
	pom(1, 0) = vector(2); pom(1, 2) = -vector(0);
	pom(2, 0) = -vector(1); pom(2, 1) = vector(0);

	return pom;
}

MatrixXd Estimator::map_gama(int n, double dt, VectorXd w)
{
	MatrixXd gama(3,3);
	
	for (int i = 0; i < 100; i++) {
		gama += pow(m_dt, i + n)*power(skew_matrix(w), i) / factorial(i + n);
	}
	return gama;
}

MatrixXd Estimator::power(MatrixXd mat, int n) {
	MatrixXd out;
	out = mat;
	for (int i = 1; i < n; i++) {
		out = out*mat;
	}
	return out;
}


long Estimator::factorial(int n)
{
	long fact = 1;

	//for Loop Block
	for (int counter = 1; counter <= n; counter++)
	{
		fact = fact * counter;
	}

	return fact;
}
VectorXd Estimator::correctX(VectorXd x_old, VectorXd Delta_x)
{
	VectorXd x_new(28);
	VectorXd  eul(3);
	Quaternionf quat, q_p, q_m;
	eul(0) = Delta_x(6);
	eul(1) = Delta_x(7);
	eul(2) = Delta_x(8);

	//q_p = Quaternionf(m_x(6), m_x(7), m_x(8), m_x(9));
	//quat = eul2quat(eul(1), eul(2), eul(3));
	//q_m = quatmultiply(quat, q_p);
	//x_new(6) = q_m.w();
	//x_new(7) = q_m.x();
	//x_new(8) = q_m.y();
	//x_new(9) = q_m.z();

	for (int i = 0; i < 6; i++) {
		x_new(i) = x_old(i) + Delta_x(i);
	}
	for (int i = 10; i < 28; i++) {
		x_new(i) = x_old(i) + Delta_x(i - 1);
	}

	x_new(6) = 0; x_new(7) = 0;	x_new(8) = 0; x_new(9) = 0;
	

	return x_new;
}

MatrixXd Estimator::correctC(VectorXd Delta_x)
{
	MatrixXd C_new(3,3), rotation(3,3);
	VectorXd  eul(3);
	eul(0) = Delta_x(6);
	eul(1) = Delta_x(7);
	eul(2) = Delta_x(8);

	rotation = eul2rotm(eul);
	C_new = rotation*m_C;

	return C_new;
}


MatrixXd Estimator::eul2rotm(VectorXd eulers)
{	
	double fi = eulers(0);
	double theta = eulers(1);
	double ksi = eulers(2);
	MatrixXd rot(3,3);


	rot(0, 0) = cos(ksi)*cos(theta);
	rot(0, 1) = cos(fi)*sin(theta)*sin(ksi) - sin(fi)*cos(ksi);
	rot(0, 2) = cos(fi)*sin(theta)*cos(ksi) + sin(fi)*sin(ksi);

	rot(1, 0) = sin(fi)*cos(theta);
	rot(1, 1) = sin(fi)*sin(theta)*sin(ksi) + cos(fi)*cos(ksi);
	rot(1, 2) = sin(fi)*sin(theta)*cos(ksi) - cos(fi)*sin(ksi);

	rot(2, 0) = -sin(theta);
	rot(2, 1) = cos(theta)*sin(ksi);
	rot(2, 2) = cos(theta)*cos(ksi);

	return rot;
}

VectorXd Estimator::make_Y(VectorXd jointPos_FL, VectorXd jointPos_FR, VectorXd jointPos_BL, VectorXd jointPos_BR, MatrixXd C, VectorXd r, VectorXd p1, VectorXd p2, VectorXd p3, VectorXd p4)
{
	VectorXd s1 = dir_kin(jointPos_FL, 0) + Vector3d (0.18, -0.12, 0);
	VectorXd s2 = dir_kin(jointPos_FR, 1) + Vector3d (0.18, 0.12, 0);
	VectorXd s3 = dir_kin(jointPos_BL, 2) + Vector3d (-0.18, -0.12, 0);
	VectorXd s4 = dir_kin(jointPos_BR, 3) + Vector3d (-0.18, 0.12, 0);

	VectorXd y1 = s1 - C*(p1 - r);
	VectorXd y2 = s2 - C*(p2 - r);
	VectorXd y3 = s3 - C*(p3 - r);
	VectorXd y4 = s4 - C*(p4 - r);

	VectorXd Y(12);


	for (int i = 0; i < 3; i++) {
		Y(i) = y1(i);
		Y(3 + i) = y2(i);
		Y(6 + i) = y3(i);
		Y(9 + i) = y4(i);
	}

	return Y;
}




MatrixXd Estimator::make_H(MatrixXd C, VectorXd r, VectorXd p1, VectorXd p2, VectorXd p3, VectorXd p4)
{
	MatrixXd H(12, 27);
	
	MatrixXd pom1 = skew_matrix(C*(p1 - r));
	MatrixXd pom2 = skew_matrix(C*(p2 - r));
	MatrixXd pom3 = skew_matrix(C*(p3 - r));
	MatrixXd pom4 = skew_matrix(C*(p4 - r));

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			H(i, j) = -C(i, j);
			H(3 + i, j) = -C(i, j);
			H(6 + i, j) = -C(i, j);
			H(9 + i, j) = -C(i, j);
			H(i, 9 + j) = C(i, j);
			H(3 + i, 12 + j) = C(i, j);
			H(6 + i, 15 + j) = C(i, j);
			H(9 + i, 18 + +j) = C(i, j);
			H(i, 6 + j) = pom1(i, j);
			H(3 + i, 6 + j) = pom2(i, j);
			H(6 + i, 6 + j) = pom3(i, j);
			H(9 + i, 6 + j) = pom4(i, j);
		}
	}
	return H;
}

MatrixXd Estimator::make_R()
{
	 MatrixXd R(12, 12);
	


	MatrixXd Rlkin1 = evaluate_jacobian_dir_kin(m_jointPos_FL, 0);
	MatrixXd Rlkin2 = evaluate_jacobian_dir_kin(m_jointPos_FR, 1);
	MatrixXd Rlkin3 = evaluate_jacobian_dir_kin(m_jointPos_BL, 2);
	MatrixXd Rlkin4 = evaluate_jacobian_dir_kin(m_jointPos_BR, 3);

	MatrixXd R1 = m_Rs1 + Rlkin1*m_Ra1*Rlkin1.transpose();
	MatrixXd R2 = m_Rs2 + Rlkin2*m_Ra2*Rlkin2.transpose();
	MatrixXd R3 = m_Rs3 + Rlkin3*m_Ra3*Rlkin3.transpose();
	MatrixXd R4 = m_Rs4 + Rlkin4*m_Ra4*Rlkin4.transpose();


	R.topLeftCorner(3, 3) = R1;
	R.bottomRightCorner(3, 3) = R4;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			R(3 + i, 3 + j) = R2(i, j);
			R(6 + i, 6 + j) = R3(i, j);
		}
	}
	return R;
}

VectorXd Estimator::dir_kin(VectorXd jointPos, int leg)
{
	double l1 = 0.18;
	double l2 = 0.09;
	double l3 = 0.05;
	double l4 = 0.06;
	double l5 = 0.1;
	double l6 = 0.15;

	double ld6 = 0.12;
	double Proj_z = l5*cos(jointPos(1)) + l6*cos(jointPos(1) + jointPos(2)); //projekcija noge na z os

	double fi = (PI - (jointPos(0))) / 2;
	double d = 2 * Proj_z*sin(jointPos(0) / 2);
	double dz = d*cos(fi);

	VectorXd FootPosition(3);	

	
	FootPosition(0) = -l5*sin(jointPos(1)) - l6*sin(jointPos(1) + jointPos(2));
	FootPosition(1) = -sin(jointPos(0))*Proj_z;		//pozitivan zakret motora pomi�e nogu u negativnom smjeru y osi
	FootPosition(2) = dz - Proj_z;

	return FootPosition;
}


MatrixXd Estimator::evaluate_jacobian_dir_kin(VectorXd jointPos, int leg)
{
	double q1 = jointPos(0);
	double q2 = jointPos(1);
	double q3 = jointPos(2);
	MatrixXd jacobian(3, 3);

	jacobian(0, 0) = 0; jacobian(0, 1) = -(3 * cos(q2 + q3)) / 20; jacobian(0, 2) = -(3 * cos(q2 + q3)) / 20;

	jacobian(1, 0) = -cos(q1)*((3 * cos(q2 + q3)) / 20 + cos(q2) / 10);    jacobian(1, 1) = (3 * sin(q2 + q3)*sin(q1)) / 20;   jacobian(1, 2) = (3 * sin(q2 + q3)*sin(q1)) / 20;

	jacobian(2, 0) = (sin(q1)*(3 * cos(q2 + q3) + 2 * cos(q2))) / 20;	jacobian(2, 1) = (3 * sin(q2 + q3)*cos(q1)) / 20;  jacobian(2, 2) = (3 * sin(q2 + q3)*cos(q1)) / 20;

	return jacobian;

}



