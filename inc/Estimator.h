#pragma once
#define PI			3.1415

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <math.h> 
#include <cmath> 

using namespace Eigen;
class Estimator
{

public:
	//double m_estimation_step;
	double m_dt;


	// Konstruktor dekonstruktor
	Estimator(double dt);
	~Estimator();


	void		Set_x0(VectorXd  value);
	VectorXd	Get_x0(void);
	void		Set_P0(MatrixXd value);
	MatrixXd	Get_P0(void);
	void		Set_Q0(MatrixXd value);
	MatrixXd	Get_Q0(void);
	void		Set_C0(MatrixXd value);
	MatrixXd	Get_C0(void);
	void		Set_R0(MatrixXd value);
	MatrixXd	Get_R0(void);
	void		Set_g(void);
	VectorXd	Get_g(void);

	void		Set_x(VectorXd value);
	VectorXd	Get_x(void);
	void		Set_P(MatrixXd value);
	MatrixXd	Get_P(void);
	void		Set_R(MatrixXd value);
	MatrixXd	Get_R(void);
	void		Set_C(MatrixXd value);
	MatrixXd	Get_C(void);

	void		Set_Ra1(MatrixXd value);
	MatrixXd	Get_Ra1(void);
	void		Set_Ra2(MatrixXd value);
	MatrixXd	Get_Ra2(void);
	void		Set_Ra3(MatrixXd value);
	MatrixXd	Get_Ra3(void);
	void		Set_Ra4(MatrixXd value);
	MatrixXd	Get_Ra4(void);
	void		Set_Rs1pom(MatrixXd value);
	MatrixXd	Get_Rs1pom(void);
	void		Set_Rs2pom(MatrixXd value);
	MatrixXd	Get_Rs2pom(void);
	void		Set_Rs3pom(MatrixXd value);
	MatrixXd	Get_Rs3pom(void);
	void		Set_Rs4pom(MatrixXd value);
	MatrixXd	Get_Rs4pom(void);
	void		Set_Qf(MatrixXd value);
	MatrixXd	Get_Qf(void);
	void		Set_Qbf(MatrixXd value);
	MatrixXd	Get_Qbf(void);
	void		Set_Qw(MatrixXd value);
	MatrixXd	Get_Qw(void);
	void		Set_Qbw(MatrixXd value);
	MatrixXd	Get_Qbw(void);
	void		Set_Qp1(MatrixXd value);
	MatrixXd	Get_Qp1(void);
	void		Set_Qp2(MatrixXd value);
	MatrixXd	Get_Qp2(void);
	void		Set_Qp3(MatrixXd value);
	MatrixXd	Get_Qp3(void);
	void		Set_Qp4(MatrixXd value);
	MatrixXd	Get_Qp4(void);


	void		Set_w_IMU(VectorXd value);
	VectorXd	Get_w_IMU(void);
	void		Set_f_IMU(VectorXd value);
	VectorXd	Get_f_IMU(void);
	void		Set_jointPos_FL(VectorXd value);
	VectorXd	Get_jointPos_FL(void);
	void		Set_jointPos_FR(VectorXd value);
	VectorXd	Get_jointPos_FR(void);
	void		Set_jointPos_BL(VectorXd value);
	VectorXd	Get_jointPos_BL(void);
	void		Set_jointPos_BR(VectorXd value);
	VectorXd	Get_jointPos_BR(void);
	void		Set_footContact(VectorXd value);
	VectorXd	Get_footContact(void);

	static VectorXd Array2Vector(int len, double *values);
	static double* Vector2Array(int len, VectorXd vector);
	static MatrixXd Array2Matrix(int rows, int cols, double *values);
	static double* Matrix2Array(int Rows, int Columns, MatrixXd vector);
	MatrixXd set_matrix(MatrixXd mat, int begin_row, int end_row, int begin_col, int end_col, MatrixXd mat2);
	MatrixXd skew_matrix(VectorXd vector);
	MatrixXd map_gama(int n, double dt, VectorXd w);
	VectorXd correctX(VectorXd x_old, VectorXd Delta_x);
	MatrixXd correctC( VectorXd Delta_x);
	MatrixXd power(MatrixXd mat, int n);
	long factorial(int n);

	// Methods for estimation
	void Prediction_step();
	void Update_step();
	void split_x(VectorXd *r, VectorXd *v, VectorXd *q, VectorXd *p1, VectorXd *p2, VectorXd *p3, VectorXd *p4, VectorXd *bf, VectorXd *bw);
	VectorXd create_x(VectorXd r, VectorXd v, VectorXd q, VectorXd p1, VectorXd p2, VectorXd p3, VectorXd p4, VectorXd bf, VectorXd bw);
	//MatrixXd quat2rotm(Quaternionf q);
	//Eigen::Quaternionf Estimator::eul2quat(const double roll, const double pitch, const double yaw);
	//Quaternionf quatmultiply(Quaternionf q, Quaternionf q2);
	MatrixXd eul2rotm(VectorXd eulers);
	VectorXd make_Y(VectorXd jointPos_FL, VectorXd jointPos_FR, VectorXd jointPos_BL, VectorXd jointPos_BR, MatrixXd C, VectorXd r, VectorXd p1, VectorXd p2, VectorXd p3, VectorXd p4);
	MatrixXd make_H(MatrixXd C, VectorXd r, VectorXd p1, VectorXd p2, VectorXd p3, VectorXd p4);
	MatrixXd make_R();
	VectorXd dir_kin(VectorXd jointPos, int leg);
	MatrixXd evaluate_jacobian_dir_kin(VectorXd jointPos, int leg);



private:
	

	// Initial states

	
	VectorXd m_x0;
	MatrixXd m_P0;
	MatrixXd m_R0;
	MatrixXd m_Q0;
	MatrixXd m_C0;
	VectorXd m_g;

	// State and Cov. matrix in current step
	VectorXd m_x;
	MatrixXd m_P;
	MatrixXd m_R;
	MatrixXd m_C;

	// Current Rs and Ra

	MatrixXd m_Ra1;
	MatrixXd m_Ra2;
	MatrixXd m_Ra3;
	MatrixXd m_Ra4;
	MatrixXd m_Rs1, m_Rs1pom;
	MatrixXd m_Rs2, m_Rs2pom;
	MatrixXd m_Rs3, m_Rs3pom;
	MatrixXd m_Rs4, m_Rs4pom;
	MatrixXd m_Qf;
	MatrixXd m_Qbf;
	MatrixXd m_Qw;
	MatrixXd m_Qbw;
	MatrixXd m_Qp1;
	MatrixXd m_Qp2;
	MatrixXd m_Qp3;
	MatrixXd m_Qp4;


	// Measurement
	VectorXd m_w_IMU;
	VectorXd m_f_IMU;
	VectorXd m_jointPos_FL;
	VectorXd m_jointPos_FR;
	VectorXd m_jointPos_BL;
	VectorXd m_jointPos_BR;
	VectorXd m_footContact;

};
